
export default class Dao {

    items = [ { id: 1, text: 'Item 1', done: true},
              { id: 2, text: 'Item 2'},
              { id: 3, text: 'Item 3'} ];
    id = 4;

    getItems() {
        return this.items;
    }

    addItem(newItem) {
        newItem.id = this.id++;

        this.items.push(newItem);
    }

    deleteItem(id) {
        this.items = this.items.filter(i => i.id !== id)
    }

    markAsDone(id) {
        this.items = this.items.map(i => {
            if (i.id === id) {
                i.done = true;
            }
            return i;
        });
    }

}
