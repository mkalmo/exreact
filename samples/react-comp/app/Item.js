import React from 'react';

const Item = props =>
    <li className="list-group-item">
        <span className={ props.item.done ? 'done' : '' }>{ props.item.text }</span>
        <div className="buttons">
            <span className="glyphicon glyphicon-ok"></span>&nbsp;
            <span className="glyphicon glyphicon-remove"></span>
        </div>
    </li>;

export default Item;
