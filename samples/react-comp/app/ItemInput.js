import React from 'react';

const inputRef = React.createRef();

function sendAndClean(callback) {
    callback(inputRef.current.value);
    inputRef.current.value = '';
}

const ItemInput = props =>
    <div>
        <div className="form-group">
            <input type="text"
                   ref={ inputRef }
                   className="form-control" />
        </div>

        <button type="button"
                onClick={() => sendAndClean(props.sendCallback)}
                className="btn btn-success">Add a Task
        </button>
    </div>;

export default ItemInput;
