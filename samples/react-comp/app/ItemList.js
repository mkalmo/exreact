import React from 'react';
import Item from './Item';

const ItemList = props =>
    <ul className="list-group ">
        {
            props.items.map(item =>
                <Item key={item.id} item={item}/>)
        }

    </ul>;

export default ItemList;
