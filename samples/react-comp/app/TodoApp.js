import React, {Component} from 'react';
import Dao from './Dao';

import ItemList from "./ItemList";
import ItemInput from "./ItemInput";

export default class TodoApp extends Component {

    state = {
        items: []
    };

    dao = new Dao();

    render() {
        return (

        <div className="col-sm-offset-3 col-sm-6">
            <h1>Todo List</h1>

            <ItemList items={this.state.items} />

            <ItemInput />
        </div>

        );
    }

    async componentDidMount() {
        await this.init();
    }

    async init() {
        const data = await this.dao.getItems();

        this.setState({ items: data });
    }

    async addItem(itemText) {
        await this.dao.addItem({ text: itemText });

        await this.init();
    }

    async markAsDone(id) {
        await this.dao.markAsDone(id);

        await this.init();
    }

    async deleteItem(id) {
        await this.dao.deleteItem(id);

        await this.init();
    }

}
