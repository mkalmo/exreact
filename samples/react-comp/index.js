import React from 'react'
import ReactDOM from 'react-dom'
import TodoApp from './app/TodoApp';

ReactDOM.render(<TodoApp />, document.getElementById('root'));