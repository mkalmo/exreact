const vm = new Vue({
    el: '#app',

    data: {
        newItemText: '',
        items: [{ id: '1', text: 'Item 1', done: true },
                { id: '2', text: 'Item 2' },
                { id: '3', text: 'Item 3' }]
    },

    methods: {
        addNewItem: () => {
            const text = vm.newItemText;
            const id = Math.max(... vm.items.map(i => i.id)) + 1;
            vm.items.push({ text, id });
        },

        markAsDone: (id) => {
            vm.items = vm.items.map(item => {
                if (item.id === id) {
                    item.done = true;
                }

                return item;
            });
        },

        deleteItem: (id) => {
            vm.items = vm.items.filter(item => item.id !== id);
        }
    }
});