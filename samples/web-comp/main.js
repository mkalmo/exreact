class PopUpInfo extends HTMLElement {
  constructor() {
    super();

    const shadow = this.attachShadow({mode: 'closed'});
    const wrapper = document.createElement('div');

    const h1 = document.createElement('h1');
    const text = this.getAttribute('data-text');
    h1.textContent = text;
    h1.onclick = function () {
        h1.textContent = h1.textContent.split("").reverse().join("");
    };

    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
    linkElem.setAttribute('href', 'component-style.css');

    shadow.appendChild(linkElem);
    shadow.appendChild(wrapper);
    wrapper.appendChild(h1);
  }
}

customElements.define('my-component', PopUpInfo);
