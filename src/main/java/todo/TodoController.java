package todo;

import org.springframework.web.bind.annotation.*;

@RestController
public class TodoController {

    private TodoDao dao;

    public TodoController(TodoDao dao) {
        this.dao = dao;
    }

    @GetMapping("api/items")
    public Iterable<Item> getTodoItems() {
        return dao.findAll();
    }

    @PostMapping("api/items")
    public Item save(@RequestBody Item item) {
        return dao.save(item);
    }

    @DeleteMapping("api/items/{id}")
    public void save(@PathVariable Long id) {
        dao.deleteById(id);
    }

    @PostMapping("api/items/{id}/mark-as-done")
    public void markAsDone(@PathVariable Long id) {
        dao.markItemAsDone(id);
    }

}
