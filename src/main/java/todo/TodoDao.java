package todo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TodoDao extends CrudRepository<Item, Long> {

    @Modifying
    @Transactional
    @Query("update Item i set i.done = true where i.id =:id")
    void markItemAsDone(@Param("id") Long id);
}
