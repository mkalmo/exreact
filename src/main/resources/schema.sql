DROP SCHEMA PUBLIC CASCADE;

create sequence hibernate_sequence start with 1;

create table item(
  id bigint primary key,
  text varchar(255),
  done boolean
);