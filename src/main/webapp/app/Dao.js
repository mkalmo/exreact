import Http from "./Http";

export default class Dao {

    http = new Http();

    getItems() {
        return this.http.get("api/items");
    }

    addItem(itemText) {
        return this.http.post("api/items", { text: itemText })
    }

    deleteItem(itemId) {
        return this.http.delete(`api/items/${itemId}`);
    }

    markAsDone(itemId) {
        return this.http.post(`api/items/${itemId}/mark-as-done`);
    }

}
