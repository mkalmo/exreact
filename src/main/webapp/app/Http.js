
export default class Http {

    get(url) {
        return fetch(url).then(response => response.json());
    }

    post(url, data) {
        return fetch(url, {
            method: 'POST',
            headers: { "Content-type": "application/json" },
            body: JSON.stringify(data)
        });
    }

    delete(url) {
        return fetch(url, { method: 'DELETE' });
    }

}
