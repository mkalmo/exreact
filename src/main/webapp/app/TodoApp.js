import React, {Component} from 'react';
import Dao from './Dao';

export default class TodoApp extends Component {

    state = {
        items: []
    };

    dao = new Dao();

    render() {
        return (
            <div>Todo Application</div>
        );
    }

    async componentDidMount() {
        await this.init();
    }

    async init() {
        const items = await this.dao.getItems();

        this.setState({ items: items });
    }

    async addItem(itemText) {
        await this.dao.addItem(itemText);

        await this.init();
    }

    async markAsDone(itemId) {
        await this.dao.markAsDone(itemId);

        await this.init();
    }

    async deleteItem(itemId) {
        await this.dao.deleteItem(itemId);

        await this.init();
    }

}
